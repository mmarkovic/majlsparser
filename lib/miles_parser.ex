defmodule MilesParser do
  import Ecto.Query

  def create_query(query) do
    case tokenize(query) do
      {:ok, tokens, _} ->
        case create_query_from_tokens(tokens) do
          "" ->
            :empty_string

          query ->
            {:ok, query}
        end

      {:error, _, _} ->
        {:error, "Invalid characters in query string."}
    end
  end

  def create_query(query, query_struct) do
    case tokenize(query) do
      {:ok, tokens, _} ->
        case create_query_from_tokens_struct(tokens, query_struct) do
          "" ->
            :empty_string

          query ->
            {:ok, query}
        end

      {:error, _, _} ->
        {:error, "Invalid characters in query string."}
    end
  end

  def tokenize(query \\ '') do
    query |> to_charlist() |> :list_lexer.string()
  end

  def create_query_from_tokens(tokens \\ []) do
    tokens
    |> Enum.with_index()
    |> Enum.reduce("", fn {{k, v}, idx}, acc ->
      case k do
        :integer ->
          acc <> " '#{v}'::float"

        :null ->
          acc <> " #{v}"

        :date ->
          acc <> " to_date('#{v}', 'MM-DD-YYYY')"

        :string ->
          acc <> " lower('#{v}')"

        :attribute ->
          case Enum.at(tokens, idx + 2) do
            {:integer, _} ->
              acc <>
                " properties->>'#{v}' ~ '^[0-9]+(\.[0-9]+)\\?$' AND (properties->>'#{v}')::float"

            _ ->
              acc <> " lower(properties->>'#{v}')"
          end

        :equals_operator ->
          case Enum.at(tokens, idx + 1) do
            {:integer, _} ->
              acc <> " #{v}"

            {:string, _} ->
              acc <> " ="

            {:null, _} ->
              acc <> " is"

            _ ->
              acc <> " #{v}"
          end

        :and_ ->
          acc <> " #{v}"

        :or_ ->
          acc <> " #{v}"

        :left_paren ->
          acc <> " #{v}"

        :right_paren ->
          acc <> " #{v}"
      end
    end)
  end

  def create_query_from_tokens_struct(tokens \\ [], query_struct) do
    tokens
    |> Enum.with_index()
    |> Enum.reduce(query_struct, fn {{k, v}, idx}, acc ->
      if k == :equals_operator do
        if idx > 2 do
          case Enum.at(tokens, idx - 2) do
            {:and_, _} ->
              case Enum.at(tokens, idx - 1) do
                {:attribute, attribute} ->
                  case Enum.at(tokens, idx + 1) do
                    {:null, _} ->
                      from(q in acc,
                        where:
                          fragment(
                            "properties->>? is null",
                            ^attribute
                          )
                      )

                    {:integer, value} ->
                      case v do
                        ">" ->
                          from(q in acc,
                            where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) > cast(? as float)",
                                ^attribute,
                                ^value
                              )
                          )

                        "<" ->
                          from(q in acc,
                            where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) < cast(? as float)",
                                ^attribute,
                                ^value
                              )
                          )

                        _ ->
                          from(q in acc,
                            where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) = cast(? as float)",
                                ^attribute,
                                ^value
                              )
                          )
                      end

                    {:date, value} ->
                      case v do
                        ">" ->
                          from(q in acc,
                            where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp > to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value
                              )
                          )

                        "<" ->
                          from(q in acc,
                            where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp < to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value
                              )
                          )

                        _ ->
                          from(q in acc,
                            where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp = to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value
                              )
                          )
                      end

                    {:string, value} ->
                      from(q in acc,
                        where:
                          fragment(
                            "lower(properties->>?) = lower(?)",
                            ^attribute,
                            ^value
                          )
                      )

                    {:left_paren, _} ->
                      {_, second_value} = Enum.at(tokens, idx + 3)

                      case Enum.at(tokens, idx + 2) do
                        {:integer, value} ->
                          from(q in acc,
                            where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) between cast(? as float) and cast(? as float)",
                                ^attribute,
                                ^value,
                                ^second_value
                              )
                          )

                        {:date, value} ->
                          from(q in acc,
                            where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp between to_timestamp(?, 'MM-DD-YYYY') and  to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value,
                                ^second_value
                              )
                          )

                        _ ->
                          acc
                      end

                    _ ->
                      acc
                  end

                _ ->
                  acc
              end

            {:or_, _} ->
              case Enum.at(tokens, idx - 1) do
                {:attribute, attribute} ->
                  case Enum.at(tokens, idx + 1) do
                    {:null, _} ->
                      from(q in acc,
                        or_where:
                          fragment(
                            "properties->>? is null",
                            ^attribute
                          )
                      )

                    {:integer, value} ->
                      case v do
                        ">" ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) > cast(? as float)",
                                ^attribute,
                                ^value
                              )
                          )

                        "<" ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) < cast(? as float)",
                                ^attribute,
                                ^value
                              )
                          )

                        _ ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) = cast(? as float)",
                                ^attribute,
                                ^value
                              )
                          )
                      end

                    {:date, value} ->
                      case v do
                        ">" ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp > to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value
                              )
                          )

                        "<" ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp < to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value
                              )
                          )

                        _ ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp = to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value
                              )
                          )
                      end

                    {:string, value} ->
                      from(q in acc,
                        or_where:
                          fragment(
                            "lower(properties->>?) = lower(?)",
                            ^attribute,
                            ^value
                          )
                      )

                    {:left_paren, _} ->
                      {_, second_value} = Enum.at(tokens, idx + 3)

                      case Enum.at(tokens, idx + 2) do
                        {:integer, value} ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "cast(NULLIF(properties->>?, '') as float) between cast(? as float) and cast(? as float)",
                                ^attribute,
                                ^value,
                                ^second_value
                              )
                          )

                        {:date, value} ->
                          from(q in acc,
                            or_where:
                              fragment(
                                "(NULLIF(properties->>?, ''))::timestamp between to_timestamp(?, 'MM-DD-YYYY') and  to_timestamp(?, 'MM-DD-YYYY')",
                                ^attribute,
                                ^value,
                                ^second_value
                              )
                          )

                        _ ->
                          acc
                      end

                    _ ->
                      acc
                  end

                _ ->
                  acc
              end

            _ ->
              acc
          end
        else
          case Enum.at(tokens, idx - 1) do
            {:attribute, attribute} ->
              case Enum.at(tokens, idx + 1) do
                {:null, _} ->
                  from(q in acc,
                    where:
                      fragment(
                        "properties->>? is null",
                        ^attribute
                      )
                  )
                {:integer, value} ->
                  case v do
                    ">" ->
                      from(q in acc,
                        where:
                          fragment(
                            "cast(NULLIF(properties->>?, '') as float) >= cast(? as float)",
                            ^attribute,
                            ^value
                          )
                      )

                    "<" ->
                      from(q in acc,
                        where:
                          fragment(
                            "cast(NULLIF(properties->>?, '') as float) <= cast(? as float)",
                            ^attribute,
                            ^value
                          )
                      )

                    _ ->
                      from(q in acc,
                        where:
                          fragment(
                            "cast(NULLIF(properties->>?, '') as float) = cast(? as float)",
                            ^attribute,
                            ^value
                          )
                      )
                  end

                {:date, value} ->
                  case v do
                    ">" ->
                      from(q in acc,
                        where:
                          fragment(
                            "(NULLIF(properties->>?, ''))::timestamp >= to_timestamp(?, 'MM-DD-YYYY')",
                            ^attribute,
                            ^value
                          )
                      )

                    "<" ->
                      from(q in acc,
                        where:
                          fragment(
                            "(NULLIF(properties->>?, ''))::timestamp <= to_timestamp(?, 'MM-DD-YYYY')",
                            ^attribute,
                            ^value
                          )
                      )

                    _ ->
                      from(q in acc,
                        where:
                          fragment(
                            "(NULLIF(properties->>?, ''))::timestamp = to_timestamp(?, 'MM-DD-YYYY')",
                            ^attribute,
                            ^value
                          )
                      )
                  end

                {:string, value} ->
                  from(q in acc,
                    where:
                      fragment(
                        "lower(properties->>?) = lower(?)",
                        ^attribute,
                        ^value
                      )
                  )

                {:left_paren, _} ->
                  {_, second_value} = Enum.at(tokens, idx + 3)

                  case Enum.at(tokens, idx + 2) do
                    {:integer, value} ->
                      from(q in acc,
                        where:
                          fragment(
                            "cast(NULLIF(properties->>?, '') as float) between cast(? as float) and cast(? as float)",
                            ^attribute,
                            ^value,
                            ^second_value
                          )
                      )

                    {:date, value} ->
                      from(q in acc,
                        where:
                          fragment(
                            "(NULLIF(properties->>?, ''))::timestamp between to_timestamp(?, 'MM-DD-YYYY') and  to_timestamp(?, 'MM-DD-YYYY')",
                            ^attribute,
                            ^value,
                            ^second_value
                          )
                      )

                    _ ->
                      acc
                  end

                _ ->
                  acc
              end

            _ ->
              acc
          end

          # first case
        end
      else
        acc
      end
    end)
  end
end
