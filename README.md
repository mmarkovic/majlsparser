# MilesParser

## End goal

This application parses pseudo sql and converts result to postgresql query.
```
Input search parameter:

(Property1 = ‘value1’ AND Property2 = ‘value2’) OR (Property3 = ‘value3’ OR Property4 in(’value4’, ‘value5’, ‘value6’) AND DateProperty in(StartDate, EndDate) OR NumberProperty in(FirstValue, SecondValue)

Output (has 2 parts):

PG Query:
select * from features where
(properties->>Property1 ILIKE %$1% AND properties->>Property2 ILIKE %$2%)
OR (properties->>Property3 ILIKE %$3% OR (properties->>Property4 ILIKE %$4% OR properties->>Property4 ILIKE %$5% OR properties->>Property4 ILIKE %$6%))
AND properties->>DateProperty >= $7 AND properties->>DateProperty < $8
OR properties->>NumberProperty >= $9 AND properties->>NumberProperty < %10

Argument list:
[value1, value2, value3, value4, value5, value6, StartDate, EndDate, FirstValue, SecondValue]
```

For reference about using leex and yecc: http://andrealeopardi.com/posts/tokenizing-and-parsing-in-elixir-using-leex-and-yecc/

**TODO: Add package instructions description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `miles_parser` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:miles_parser, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/miles_parser](https://hexdocs.pm/miles_parser).

