defmodule MilesParserTest do
  use ExUnit.Case
  import Ecto.Query
  doctest MilesParser

  test "should tokenize the query string" do
    query_string = "PROGRESS_STATUS = null AND (VOLTAGE > 12 OR VOLTAGE = 69)"

    tokenized_string = [
      attribute: "PROGRESS_STATUS",
      equals_operator: "=",
      null: "null",
      and_: "AND",
      left_paren: "(",
      attribute: "VOLTAGE",
      equals_operator: ">",
      integer: 12,
      or_: "OR",
      attribute: "VOLTAGE",
      equals_operator: "=",
      integer: 69,
      right_paren: ")"
    ]

    {:ok, tokens, _} = MilesParser.tokenize(query_string)
    assert tokens == tokenized_string
  end

  test "should tokenize empty string" do
    assert MilesParser.tokenize() === {:ok, [], 1}
  end

  test "should throw an error when ';' character is sent for sql safety" do
    string = "PROGRESS_STATUS; = null AND (VOLTAGE = 12 OR VOLTAGE = 69)"
    assert {:error, {_, :list_lexer, {:illegal, ';'}}, _} = MilesParser.tokenize(string)
  end

  test "should create a pg query out of given tokens" do
    query =
      "PROGRESS_STATUS = null AND (VOLTAGE = 12 OR VOLTAGE = 69) AND COMMENT = 'sometext' AND CREATED_AT = 10-05-1989"

    result =
      " lower(properties->>'PROGRESS_STATUS') is null AND ( properties->>'VOLTAGE' ~ '^[0-9]+(.[0-9]+)\\?$' AND (properties->>'VOLTAGE')::float = '12'::float OR properties->>'VOLTAGE' ~ '^[0-9]+(.[0-9]+)\\?$' AND (properties->>'VOLTAGE')::float = '69'::float ) AND lower(properties->>'COMMENT') = lower('sometext') AND lower(properties->>'CREATED_AT') = to_date('10-05-1989', 'MM-DD-YYYY')"

    {:ok, tokens, _} = MilesParser.tokenize(query)
    assert result == MilesParser.create_query_from_tokens(tokens)
  end

  test "should execute tokenize and create_query_from_tokens for a given function" do
    query = "PROGRESS_STATUS = null AND (VOLTAGE = 12 OR VOLTAGE = 69) AND COMMENT = 'sometext'"

    result =
      " lower(properties->>'PROGRESS_STATUS') is null AND ( properties->>'VOLTAGE' ~ '^[0-9]+(.[0-9]+)\\?$' AND (properties->>'VOLTAGE')::float = '12'::float OR properties->>'VOLTAGE' ~ '^[0-9]+(.[0-9]+)\\?$' AND (properties->>'VOLTAGE')::float = '69'::float ) AND lower(properties->>'COMMENT') = lower('sometext')"

    assert {:ok, result} == MilesParser.create_query(query)
  end

  test "should throw an error if invalid characters are passed" do
    query = "PROGRESS_STATUS = null; AND (VOLTAGE = 12 OR VOLTAGE = 69) AND COMMENT = 'sometext'"
    assert {:error, "Invalid characters in query string."} = MilesParser.create_query(query)
  end

  test "should return empty for empty query string" do
    assert :empty_string = MilesParser.create_query("")
  end

  test "should tokenize date MM-DD-YYYY" do
    query_string = "created_at = 10-05-1989"

    tokenized_string = [
      attribute: "created_at",
      equals_operator: "=",
      date: "10-05-1989"
    ]

    {:ok, tokens, _} = MilesParser.tokenize(query_string)
    assert tokens == tokenized_string
  end

  test "new query builder" do
    query = from(f in "features")

    MilesParser.create_query(
      "PROGRESS_STATUS = null AND VOLTAGE > 12 OR VOLTAGE = 69",
      query
    )
  end

  test "dates in new query builder" do
    query = from(f in "features")

    MilesParser.create_query(
      "GDB_FROM_DATE = (09-02-2018 06-30-2018)",
      query
    )
  end
end
