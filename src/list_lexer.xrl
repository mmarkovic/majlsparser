Definitions.
OR                      = (O|o)(R|r)
AND                     = (A|a)(N|n)(D|d)
IN                      = (I|i)(N|n)
ATTRIBUTE               = ([a-zA-Z][a-zA-Z0-9_\-]*)
VALUE_DOUBLE_QUOTE      = \"(\"\"|[^\"\n])*\"
VALUE_SINGLE_QUOTE      = \'(\'\'|[^\'\n])*\'
WHITESPACE              = [\s\t\n\r]
COMMA                   = (,)
INT                     = (\-*[0-9]+)
NULL                    = (N|n)(U|u)(L|l)(L|l)
DATE                    = ([0-9]+)\-([0-9]+)\-([0-9]+)
EQ                      = (=)
NE                      = (!=)
GT                      = (>)
GTE                     = (>=)
LT                      = (<)
LTE                     = (<=)
OPEN                    = \(
CLOSE                   = \)
Rules.
{AND}                   : {token, {and_, list_to_binary(TokenChars)}}.
{OR}                    : {token, {or_, list_to_binary(TokenChars)}}.
{NULL}                  : {token, {null, list_to_binary(TokenChars)}}.
{IN}                    : {token, {equals_operator, list_to_binary(TokenChars)}}.
{ATTRIBUTE}             : {token, {attribute, clean_up_identifier(TokenChars)}}.
{DATE}                  : {token, {date, list_to_binary(TokenChars)}}.
{VALUE_DOUBLE_QUOTE}    : {token, {string, clean_up_identifier(TokenChars)}}.
{VALUE_SINGLE_QUOTE}    : {token, {string, clean_up_identifier(TokenChars)}}.
{WHITESPACE}+           : skip_token.
{COMMA}                 : skip_token.
{INT}                   : {token, {integer, list_to_integer(TokenChars)}}.
{EQ}                    : {token, {equals_operator, list_to_binary(TokenChars)}}.
{GT}                    : {token, {equals_operator, list_to_binary(TokenChars)}}.
{GTE}                   : {token, {equals_operator, list_to_binary(TokenChars)}}.
{LT}                    : {token, {equals_operator, list_to_binary(TokenChars)}}.
{LTE}                   : {token, {equals_operator, list_to_binary(TokenChars)}}.
{OPEN}                  : {token, {left_paren, list_to_binary(TokenChars)}}.
{CLOSE}                 : {token, {right_paren, list_to_binary(TokenChars)}}.
Erlang code.
clean_up_identifier(Literal) ->
    clean_up_literal(Literal).
clean_up_literal(Literal) ->
    Literal1 = case hd(Literal) of
        $' -> accurate_strip(Literal, $');
        $" ->
            [error(unicode_in_quotes) || U <- Literal, U > 127],
            accurate_strip(Literal, $");
        _ -> Literal
    end,
    DeDupedInternalQuotes = dedup_quotes(Literal1),
    list_to_binary(DeDupedInternalQuotes).
%% dedup(licate) quotes, using pattern matching to reduce to O(n)
dedup_quotes(S) ->
    dedup_quotes(S, []).
dedup_quotes([], Acc) ->
    lists:reverse(Acc);
dedup_quotes([H0,H1|T], Acc) when H0 =:= $' andalso H1 =:= $' ->
    dedup_quotes(T, [H0|Acc]);
dedup_quotes([H0,H1|T], Acc) when H0 =:= $" andalso H1 =:= $" ->
    dedup_quotes(T, [H0|Acc]);
dedup_quotes([H|T], Acc) ->
    dedup_quotes(T, [H|Acc]).
%% only strip one quote, to accept Literals ending in the quote
%% character being stripped
accurate_strip(S, C) ->
    case {hd(S), lists:last(S), length(S)} of
        {C, C, Len} when Len > 1 ->
            string:substr(S, 2, Len - 2);
        _ ->
            S
    end.